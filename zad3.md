---
author: Bartosz Dziuba
title: Diuna
subtitle: Religia i władza
date: 
theme: Warsaw
output: beamer_presentation
header-includes: 
    \usepackage{xcolor}
    \usepackage{listings}
---

#   Wstęp

**Diuna** to jednak z pierwszych książek science-fiction, która porusza temat religii. W przeciwieństwie do większości pozostałych pisarzy gatunku (których twórczość zazwyczaj przedstawia religię będąca w zaniku), koncepcja Franka Herberta pokazuje przyszłość, w której religia, jak i system feudalny, pełnią bardzo istotną rolę w życiu codziennym i polityce.

Religia w powieści reprezentuje źródło pocieszenia i władzy. Główny bohater - **Paul Muad'Dib** - posiada wiele umiejętności, które wynoszą go ponad _zwykłych śmiertelników_, ale to właśnie bezwzględne wykorzystanie mitu i religii staje się najważniejszym narzędziem w jego rękach. 

\begin{center}
Inspirowany przez niego ślepy fanatyzm pozwala mu nie tylko przetrwać, ale i wbrew wszelkim oczekiwaniom przejąć kontrolę nad znanym, zamieszkałym wszechświatem. 
\end{center}

\textcolor{red}{Jego mit jest tak potężny, że ostatecznie sam traci nad nim kontrolę.}

#   Kilka faktów


Lista książek w *oryginalnej* serii napisanej za życia Franka Herberta:


1. Diuna
2. Mesjasz Diuny
3. Dzieci Diuny
4. Bóg Imperator Diuny
5. Heretycy Diuny
6. Diuna: Kapitularz

Ważne rody i frakcje:


- Rody:
    * Ród Atrydów
    * Ród Korrino
    * Ród Harkonnenów
- Organizacje:
    * Bene Gesserit
    * Bene Tleilax
    * Ixianie

#   Bene Gesserit


### 
Zakon wyłącznie kobiecy o ogromnych wpływach w Cesarstwie. Siostry specjalizują się w sterowaniu programem eugenicznym, który ma zaowocować męskim Bene Gesserit - Kwisatz Haderach, który spojrzy tam, gdzie one nie mogą.

Siostry zakonu posiadają wiele umiejętności - w tym pełną kontrolę nad ciałem, korzystanie ze skumulowanych wspomnień swoich przodków oraz tajne techniki walki wręcz.

### Pani Jessica
Jessica, matka Paula, należy do Bene Gesserit. Wbrew rozkazom uczy syna tajemnic zakonu. 

###
Jessica krzyżuje wielopokoleniowe plany Bene Gesserit rodząc Paula wbrew rozkazom. Czyni to z miłości do księcia Leto Atrydy, którego jest konkubiną. 

#   Otoczenie Paula z młodości

\begin{alertblock}{Doktor Yueh}
Doktor z akademii Suuk, oddany człowiek księcia, nauczyciel Paula. Niestety, ktoś znajduje sposób, aby obejść jego zakodowane warunkowanie i staje się zdrajcą.
\end{alertblock}

Bliżsi przyjaciele Paula:

\begin{block}{Duncan Idaho}
Mistrz miecza Domu Atrydów, bliski przyjaciel Paula. Odgrywa później ważniejszą rolę, niż można się było spodziewać...
\end{block}

\begin{exampleblock}{Gurney Halleck}
Starszy nauczyciel szermierki, poeta. Członek Domu o dozgonnej lojalności.
\end{exampleblock}

#   Duncan vs Gurney - blok kodu


Kod w pythonie obliczający wynik sparingu między Duncanem a Gurney'em, zgodnie ze zdaniem Duncana:


```python
x = random.randrange(1, 10)
if (x <= 6)
    print "Gurney Halleck wins"
else
    print "Duncan Idaho wins"
```

#   Duncan vs Gurney - kod Latex {.fragile}


\begin{lstlisting}[language=python, basicstyle=\scriptsize\ttfamily,identifierstyle=\color{blue},]
if (x <= 6)
    print "Gurney Halleck wins"
else
    print "Duncan Idaho wins"
\end{lstlisting}

#   Duncan vs Gurney - kod z pliku


\lstinputlisting[language=csh, linerange={2-4}]{src/kod.cs}

#   Ważniejsze religie

::: {.columns}
:::: {.column width=0.3}
Kult Muad'Diba
::::
:::: {.column width=0.3}
Zensunnici
::::
:::: {.column width=0.3}
Chrześcijaństwo Mahayana
::::
:::

#   Artwork - Muad'Dib


![](img/muaddib.jpg){ height=90% width=90%}

#   Artwork - Fremenka


![](img/fremen.jpg){ height=75% width=75%}

#   Artwork - Mur Zaporowy


![](img/shieldwall.jpg)


# Tabela

| Tabelka       | Żeby          | Jakaś      | Była |
| ------------- |:-------------:| -----:     | ---- |
| col 3 is      | right-aligned | $1600      |   @  |
| col 2 is      | centered      |   $12      |   +  |
| czerwie       | są            |gigantyczne |   l  |