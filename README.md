#   Zadanie 3 – 4pkt
*Pobrać repozytorium https://gitlab.com/mniewins66/templatemn.git z templatem prezentacji.
*Podjąć próbę konwersji pliku do formatu pdf.*Korzystając z tego wzorca przygotować własną prezentację na wybrany samodzielnie temat np ulubiony zespół muzyczny, ulubiona książka, ulubione miejsce na ziemi itd.
*Postępy prac nad prezentacją rejestrować jako kolejne commity.  Podczas oceny będzie brana pod uwagę liczba commitów jak i jasność przypisanych im komunikatów.
*Następnie repozytorium umieścić na serwerze gitlab w projekcie o nazwie zad3. Do projektu jako użytkownika dodać usera mniewins66.
*Jako wynik pracy przesłać link do projektu na serwerze gitlab lub opcjonalnie spakowane repozytorium lokalne wysłać e-mailem do prowadzącego (lub umieścićw zasobach kursu)

